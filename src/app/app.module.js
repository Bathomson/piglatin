import angular from 'angular';
import 'normalize.css';

import AppComponent from './app.component';
import ComponentsModule from './components/components.module';
import CommonModule from './common/common.module';
import './app.scss';

const AppModule = angular
  .module('app', [
    ComponentsModule,
    CommonModule
  ])
  .component('app', AppComponent)
  .name;

export default AppModule;
