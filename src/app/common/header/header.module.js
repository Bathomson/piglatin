// Vendor
import angular from 'angular';
import uiRouter from 'angular-ui-router';

// App
import HeaderComponent from './header.component';
import './header.scss';

const HeaderModule = angular
  .module('header', [
    uiRouter
  ])
  .component('bwHeader', HeaderComponent)
  .name;

export default HeaderModule;
