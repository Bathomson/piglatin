import angular from 'angular';
import uiRouter from 'angular-ui-router';

import PigLatinComponent from './pig-latin.component';
import PigLatinService from './pig-latin.service';
import './pig-latin.scss';

import PigLatinFormModule from './pig-latin-form/pig-latin-form.module';
import PigLatinListModule from './pig-latin-list/pig-latin-list.module';

const PigLatinModule = angular
  .module('pigLatin', [
    uiRouter,
    PigLatinFormModule,
    PigLatinListModule
  ])
  .component('pigLatin', PigLatinComponent)
  .service('PigLatinService', PigLatinService)
  .config(($stateProvider, $urlRouterProvider) => {
    'ngInject';
    $stateProvider
      .state('pigLatin', {
        url: '/pig-latin',
        component: 'pigLatin'
      });
    $urlRouterProvider.otherwise('/pig-latin');
  })
  .name;

export default PigLatinModule;
