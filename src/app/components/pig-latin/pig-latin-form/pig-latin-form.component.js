import template from './pig-latin-form.html';

class PigLatinFormController {
  constructor(EventEmitter) {
    'ngInject';
    this.EventEmitter = EventEmitter;
  }
  $onChanges(changes) {
    if (changes.sentence) {
      this.sentence = Object.assign({}, changes.sentence.currentValue);
    }
  }
  onSubmit() {
    if (!this.sentence.original) {
      return;
    }
    this.onAddSentence(
      this.EventEmitter({
        sentence: this.sentence
      })
    );
  }
}

const PigLatinFormComponent = {
  bindings: {
    sentence: '<',
    onAddSentence: '&'
  },
  template,
  controller: PigLatinFormController
};

export default PigLatinFormComponent;
