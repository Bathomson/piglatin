import angular from 'angular';

import PigLatinFormComponent from './pig-latin-form.component';
import './pig-latin-form.scss';

const PigLatinFormModule = angular
  .module('pigLatin.pigLatinForm', [])
  .component('pigLatinForm', PigLatinFormComponent)
  .value('EventEmitter', payload => ({$event: payload}))
  .name;

export default PigLatinFormModule;
