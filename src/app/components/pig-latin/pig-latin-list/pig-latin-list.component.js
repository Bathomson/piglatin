import template from './pig-latin-list.html';

class PigLatinListController {
  constructor(EventEmitter) {
    'ngInject';
    this.EventEmitter = EventEmitter;
  }
  $onChanges(changes) {
    if (changes.sentenceList) {
      this.sentences = Object.assign({}, changes.sentenceList.currentValue);
    }
  }
}

const PigLatinListComponent = {
  bindings: {
    sentenceList: '<'
  },
  template,
  controller: PigLatinListController
};

export default PigLatinListComponent;
