import angular from 'angular';

import PigLatinListComponent from './pig-latin-list.component';
import './pig-latin-list.scss';

const PigLatinListModule = angular
  .module('pigLatin.pigLatinList', [])
  .component('pigLatinList', PigLatinListComponent)
  .value('EventEmitter', payload => ({$event: payload}))
  .name;

export default PigLatinListModule;
