import template from './pig-latin.html';

class PigLatinController {
  constructor(PigLatinService, $log) {
    'ngInject';
    this.pigLatinService = PigLatinService;
    this.limit = 3;
    this.$log = $log;
  }
  $onInit() {
    this.newSentence = {
      original: ''
    };
    this.sentences = [];
  }
  addSentence({sentence}) {
    if (!sentence.original) {
      return;
    }

    const converted = this.pigLatinService.convert(sentence.original);
    const newSentence = Object.assign({converted}, sentence);

    this.sentences.unshift(newSentence);

    if (this.sentences.length > this.limit) {
      this.sentences.pop();
    }

    this.sentences = this.sentences.slice();

    this.newSentence = {
      original: ''
    };
  }
}

const PigLatinComponent = {
  template,
  controller: PigLatinController
};

export default PigLatinComponent;
