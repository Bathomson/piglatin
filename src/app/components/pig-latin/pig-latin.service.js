class PigLatinService {
  constructor($log) {
    'ngInject';
    this.$log = $log;
  }
  convert(sentence) {
    const words = sentence.split(' ');
    const convertedWords = words.map(this.convertWord);
    return convertedWords.join(' ');
  }
  convertWord(word) {
    // Regex to test if letter is vowel
    const vowelRegex = /[aeiou]/i;

    // If begin with vowel sounds add 'way' to the end
    if (vowelRegex.test(word[0])) {
      return word + 'way';
    }

    // Split word into array of letters
    const letters = word.split('');

    // Flag to determin if the first letter is uppercase
    // const isFirstUpper = letters[0] === letters[0].toUpperCase();

    // Find index of first vowel
    let vowelIndex = letters.find(letter => {
      return Boolean(vowelRegex.test(letter));
    });
    vowelIndex = letters.indexOf(vowelIndex);

    // End letters
    let endLetters = [];

    // If there is a vowel in the word then move letters before it to end
    if (vowelIndex > -1) {
      endLetters = letters.splice(0, vowelIndex);
      endLetters.reverse();
    }

    // Build final word with "ay" on the end
    let converted = letters.concat(endLetters).join('') + 'ay';

    // // Make first letter uppercase if needed
    // if (isFirstUpper) {
    //   converted = converted[0].toUpperCase() + converted.slice(1);
    // }
    converted = converted.toLowerCase();

    // Return final string
    return converted.charAt(0).toUpperCase() + converted.slice(1);
  }
}

export default PigLatinService;
