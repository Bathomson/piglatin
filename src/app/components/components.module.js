import angular from 'angular';

import PigLatinModule from './pig-latin/pig-latin.module';

const ComponentsModule = angular
  .module('app.components', [
    PigLatinModule
  ])
  .name;

export default ComponentsModule;
