## 1. Getting Started
------
Install dependencies
```sh
$ yarn install
```
or
```sh
$ npm install
```
Serve the application
```sh
$ gulp serve
```
In your terminal it should show you the URL to open, something like: http://localhost:3000/